libnet-openssh-perl (0.84-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.84.

 -- gregor herrmann <gregoa@debian.org>  Mon, 04 Sep 2023 20:16:10 +0200

libnet-openssh-perl (0.83-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.83.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Add empty debian/tests/pkg-perl/syntax-skip
    to enable autopkgtest's syntax.t.
  * Update Recommends and Suggests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Feb 2023 20:23:23 +0100

libnet-openssh-perl (0.82-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + libnet-openssh-perl: Add Multi-Arch: foreign.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.82.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Mar 2022 18:56:25 +0100

libnet-openssh-perl (0.80-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.80.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Tue, 29 Sep 2020 20:41:37 +0200

libnet-openssh-perl (0.79-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 0.79.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop libnet-openssh-gateway-perl from Suggests, doesn't exist.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 23:02:29 +0200

libnet-openssh-perl (0.78-1) unstable; urgency=medium

  * New upstream version 0.78

 -- Florian Schlichting <fsfs@debian.org>  Thu, 21 Jun 2018 20:14:07 +0200

libnet-openssh-perl (0.77-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.77.
  * Drop spelling.patch, applied upstream.
  * Update years of upstream copyright.
  * debian/libnet-openssh-perl.examples: sample/ has been renamed to
    examples/.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Mon, 09 Apr 2018 20:57:58 +0200

libnet-openssh-perl (0.74-1) unstable; urgency=medium

  * Import upstream version 0.74
  * Update copyright years
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 25 Sep 2017 22:29:47 +0200

libnet-openssh-perl (0.73-1) unstable; urgency=medium

  * Team upload.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Import upstream version 0.73.
  * Add a patch to fix a spelling mistake in the POD.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Jul 2016 20:47:35 +0200

libnet-openssh-perl (0.70-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 0.70.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Feb 2016 17:27:01 +0100

libnet-openssh-perl (0.68-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.68.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 Dec 2015 18:40:41 +0100

libnet-openssh-perl (0.66-1) unstable; urgency=medium

  * Imported Upstream version 0.66
  * Drop pod-wording.patch, applied upstream
  * Bump dh compat to level 9

 -- Florian Schlichting <fsfs@debian.org>  Thu, 15 Oct 2015 23:39:09 +0200

libnet-openssh-perl (0.64-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Convert packaging to git-debcherry
  * Add debian/upstream/metadata
  * Import Upstream version 0.64
  * Update copyright years
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able
  * Drop old and create new pod-wording.patch

 -- Florian Schlichting <fsfs@debian.org>  Tue, 18 Aug 2015 18:58:06 +0200

libnet-openssh-perl (0.62-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of upstream copyright.
  * Drop spelling.patch, fixed upstream.
  * Add patch to improve wording in POD.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jun 2014 14:55:17 +0200

libnet-openssh-perl (0.60-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Imported Upstream version 0.60
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Bumped Standards-Version to 3.9.4 (use copyright-format 1.0)
  * Bumped copyright years
  * Dropped fix_pod_spelling.patch, applied upstream
  * New spelling.patch

 -- Florian Schlichting <fsfs@debian.org>  Tue, 30 Apr 2013 23:44:13 +0200

libnet-openssh-perl (0.57-1) unstable; urgency=low

  * Initial Release. (Closes: #660923)

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Wed, 22 Feb 2012 23:35:55 +0100
